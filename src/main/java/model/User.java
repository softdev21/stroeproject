/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author commis
 */
public class User {
    private int id;
    private String name;
    private String password;
    private String tel;

    public User(int id, String name, String password, String tel) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.tel = tel;
    }
    
    public User(String name, String password, String tel) {
        this(-1,name,password,tel);
    }
    
     public User(String name, String tel) {
        this(-1,name,"",tel);
    }
     
     public User(int id, String name, String tel) {
        this(id ,name ,"" ,tel);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", password=" + password + ", tel=" + tel + '}';
    }
    
}
